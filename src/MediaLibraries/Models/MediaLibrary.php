<?php

namespace EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Models;

use App\Source\Media\Enums\MediaTypeEnum;
use EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Enums\MediaConversionEnum;
use EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Enums\MediaLibraryCustomPropertiesEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Translatable\HasTranslations;
use stdClass;

class MediaLibrary extends Model implements HasMedia
{
    use HasTranslations,
        InteractsWithMedia;

    public $timestamps = true;

    public $fillable = [
        'name',
        'collection_name',
        'description',
        'custom_properties',
        'conversions',
        'media_library_type_id',
        'active',
    ];

    public $hidden = [
        'created_at',
        'updated_at',
    ];

    public array $translatable = [
        'name',
        'description',
    ];

    protected $casts = [
        'custom_properties' => 'json',
        'conversions' => 'json',
    ];

    protected $with = [
        'type',
    ];

    /**
     * @var bool
     *
     */
    public $registerMediaConversionsUsingModelInstance = true;

    /**
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(MediaLibraryType::class, "media_library_type_id",);
    }

    /**
     * @return MorphMany
     */
    public function media(): MorphMany
    {
        return $this->morphMany(config('media-library.media_model'), 'model');
    }

    /**
     * @return HasMany
     */
    public function collectionMedia(): HasMany
    {
        return $this->hasMany(config('media-library.media_model'), 'collection_name', 'collection_name');
    }

    /**
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection($this->getCollectionName())
            ->useDisk(env('MEDIA_COLLECTIONS_DISK', 's3'));
    }

    /**
     * @param $media
     * @return void
     */
    public function registerMediaConversions($media = null): void
    {
        $this->registerMediaConversionsForModel($this);
    }

    /**
     * @param HasMedia $model
     * @return void
     */
    public function registerMediaConversionsForModel(HasMedia $model): void
    {
        foreach ($this->getConversions() as $conversion_enum) {
            $conversion = MediaConversionEnum::tryFrom($conversion_enum)->getConversion();
            $conversion->get($model, $this->getCollectionName());
        }
    }

    /**
     * @return string
     */
    public function getCollectionName(): string
    {
        return $this->collection_name ?? "default";
    }

    /**
     * @return array
     */
    public function getConversions(): array
    {
        return $this->conversions ?? [];
    }

    /**
     * @param $field
     * @return mixed|void
     */
    public function getCustomProperties($field)
    {
        if (MediaLibraryCustomPropertiesEnum::tryFrom($field)) {
            if (is_array($this->custom_properties)) {
                if (array_key_exists($field, $this->custom_properties))
                    return $this->custom_properties[$field];
            } elseif (is_object($this->custom_properties)) {
                if (property_exists($this->custom_properties, $field)) {
                    return $this->custom_properties->$field;
                }
            }
        }
    }

    public function getCustomPropertiesAttribute($field)
    {
        $data = json_decode($field, true);
        return count($data) ? (object)$data : new stdClass();
    }

    /**
     * @return bool
     */
    public function isImage(): bool
    {
        return $this->type->library_type === MediaTypeEnum::images->name;
    }
}