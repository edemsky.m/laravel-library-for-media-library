<?php

namespace EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Translatable\HasTranslations;

class MediaLibraryType extends Model
{
    use HasTranslations;

    public $timestamps = true;

    public $fillable = [
        'name',
        'description',
        'slug',
        'mime_types',
    ];

    public $hidden = [
        'created_at',
        'updated_at',
    ];

    public array $translatable = [
        'name',
    ];
    protected $casts = [
        'mime_types' => 'json',
    ];
    protected $with = [
        'statuses',
    ];

    public function libraries(): HasMany
    {
        return $this->hasMany(MediaLibrary::class);
    }

}