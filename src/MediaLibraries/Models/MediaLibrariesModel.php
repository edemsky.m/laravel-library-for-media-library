<?php

namespace EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Models;

use Illuminate\Database\Eloquent\Model;

class MediaLibrariesModel extends Model
{
    protected $table="media_libraries_model";
}