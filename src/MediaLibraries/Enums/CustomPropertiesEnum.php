<?php

namespace EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Enums;

enum CustomPropertiesEnum: string
{

    case Status = "status";
    case Name = "name";
    case Description = "description";
    case Alt = "alt";
    case Geo = "geo";
    case Date = "date";
    case Slug = "slug";

}