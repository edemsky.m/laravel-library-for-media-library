<?php

namespace App\Source\Media\Enums;

enum MediaTypeEnum: string
{
    case videos = "Videos";
    case files = "Files";
    case images = "Images";
}