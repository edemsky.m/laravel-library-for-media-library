<?php

namespace EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Enums;

use EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Contracts\MediaConversionInterface;
use EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Conversions\DefaultConversion;

enum MediaConversionEnum: string
{

    case Default = "Default";


    public function getConversion(): MediaConversionInterface
    {
        return match ($this) {
            self::Default => new DefaultConversion(),
        };
    }
}