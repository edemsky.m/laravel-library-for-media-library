<?php

namespace App\Source\Media\Enums;

enum MimeTypesEnum: string
{

    case aac = "audio/aac";
    case abw = "application/x-abiword";
    case arc = "application/x-freearc";
    case avi = "video/x-msvideo";
    case azw = "application/vnd.amazon.ebook";
    case bin = "application/octet-stream";
    case bmp = "image/bmp";
    case bz = "application/x-bzip";
    case bz2 = "application/x-bzip2";
    case csh = "application/x-csh";
    case css = "text/css";
    case csv = "text/csv";
    case doc = "application/msword";
    case docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    case eot = "application/vnd.ms-fontobject";
    case epub = "application/epub+zip";
    case gz = "application/gzip";
    case gif = "image/gif";
    case html = "text/html";
    case ico = "image/vnd.microsoft.icon";
    case ics = "text/calendar";
    case jar = "application/java-archive";
    case jpeg = "image/jpeg";
    case js = "text/javascript";
    case json = "application/json";
    case jsonld = "application/ld+json";
    case mid = "audio/midi";
    case midi = "audio/x-midi";
    case mp3 = "audio/mpeg";
    case mpeg = "video/mpeg";
    case mpkg = "application/vnd.apple.installer+xml";
    case odp = "application/vnd.oasis.opendocument.presentation";
    case ods = "application/vnd.oasis.opendocument.spreadsheet";
    case odt = "application/vnd.oasis.opendocument.text";
    case oga = "audio/ogg";
    case ogv = "video/ogg";
    case ogx = "application/ogg";
    case opus = "audio/opus";
    case otf = "font/otf";
    case png = "image/png";
    case pdf = "application/pdf";
    case php = "application/php";
    case ppt = "application/vnd.ms-powerpoint";
    case pptx = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    case rar = "application/vnd.rar";
    case rtf = "application/rtf";
    case sh = "application/x-sh";
    case svg = "image/svg+xml";
    case swf = "application/x-shockwave-flash";
    case tar = "application/x-tar";
    case tif = "image/tiff";
    case ts = "video/mp2t";
    case ttf = "font/ttf";
    case txt = "text/plain";
    case vsd = "application/vnd.visio";
    case wav = "audio/wav";
    case weba = "audio/webm";
    case webm = "video/webm";
    case webp = "image/webp";
    case woff = "font/woff";
    case woff2 = "font/woff2";
    case xhtml = "application/xhtml+xml";
    case xls = "application/vnd.ms-excel";
    case xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    case xml = "application/xml";
    case atom_xml = "application/atom+xml";
    case xul = "application/vnd.mozilla.xul+xml";
    case zip = "application/zip";
    case video3gp = "video/3gpp";
    case video3g2 = "video/3gpp2";
    case x7z = "application/x-7z-compressed";
    case mp4 = "video/mp4";
    case mov = "video/quicktime";
    case mkv = "video/x-matroska";
    case h264 = "video/h264";
    case h261 = "video/h261";
    case h263 = "video/h263";


    public function extensions(): array
    {
        return match ($this) {
            self::jpeg => ['jpeg', 'jpg', 'jpe'],
            self::xls => ['xls', 'xlm', 'xla', 'xlc', 'xlt', 'xlw'],
            self::mp3 => ['mpga', 'mp2', 'mp2a', 'mp3', 'm2a', 'm3a'],
            self::svg => ['svg', 'svgz'],
            self::tif => ['tiff', 'tif'],
            self::txt => ['txt', 'text', 'conf', 'def', 'list', 'log', 'in'],
            self::mp4 => ['mp4', 'mp4v', 'mpg4'],
            self::mpeg => ['mpeg', 'mpg', 'mpe', 'm1v', 'm2v'],
            self::mov => ['qt', 'mov'],
            self::mkv => ['mkv', 'mk3d', 'mks'],
            default => [$this->name]
        };
    }

    public static function toExtensionsArray(): array
    {
        $extensions = [];
        foreach (self::cases() as $case) {
            $extensions[$case->name] = $case->extensions();
        }
        return $extensions;
    }

}