<?php

namespace EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Enums;

enum MediaLibraryCustomPropertiesEnum: string
{

    case isImage = "isImage";
    case minFileSize = "minFileSize";
    case maxFileSize = "maxFileSize";
    case minDimension = "minDimension";
    case maxDimension = "maxDimension";
    case forAttachedMedia = "forAttachedMedia";


    public function getNames(): array
    {
        return match ($this) {
            self::isImage => ['ru' => 'Библиотека изображений', 'en' => 'Image Library'],
            self::minFileSize => ['ru' => 'Минимальный размер файла', 'en' => 'Minimum file size'],
            self::maxFileSize => ['ru' => 'Максимальный размер файлв', 'en' => 'Maximum file size'],
            self::minDimension => ['ru' => 'Минимальное разрешение изображения', 'en' => 'Minimum image resolution'],
            self::maxDimension => ['ru' => 'Максимальное  разрешение изображения', 'en' => 'Maximum image resolution'],
            self::forAttachedMedia => ['ru' => 'Использовать для прикрепления медиа', 'en' => 'Use to attach media'],
        };
    }

}