<?php

namespace EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Contracts;


use Spatie\MediaLibrary\HasMedia;

interface MediaConversionInterface
{
    public function get(HasMedia $model, string $collection): void;
}