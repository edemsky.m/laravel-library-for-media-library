<?php

namespace EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Conversions;

use EdemskyM\LaravelLibraryForMediaLibrary\MediaLibraries\Contracts\MediaConversionInterface;
use Spatie\Image\Enums\Fit;
use Spatie\MediaLibrary\HasMedia;

class DefaultConversion implements MediaConversionInterface
{

    public function get(HasMedia $model, string $collection = "default"): void
    {
        $model->addMediaConversion('default')
            ->performOnCollections($collection)
            ->fit(Fit::Fill, 500, 500);
    }
}